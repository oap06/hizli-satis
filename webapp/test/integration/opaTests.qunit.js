/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"zcrm/zcrm_sales_order_operation/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});